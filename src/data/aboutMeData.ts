export const aboutMeData = {
    ru: {
        subTitle: 'Информация о учебе, работе и навыках',
        title: 'Обо мне',
        description: `Доброго дня. Я фронтенд-разработчик, с опытом создания SPA, используя React, Redux, TypeScript.
         Постоянно развиваю свои знания в глубь и в ширину, на данный момент разрабатываю front-end часть проекта, а также активно изучаю написание back-end.
          Привык качественно и ответственно подходить к своей работе, всегда доводить дело до конца.`,
        nameTabs: ['Главные навыки', 'Образование', 'Опыт работы'],
        workExperience: {
            frontEnd: {
                title: 'Мои работы как Front-end разработчика:',
                projectCards: 'Проект: Обучающие карточки',
                projectSocialNetwork: 'Проект: Социальная сеть',
                projectTodolist: 'Проект: Список дел',
                projectRestaurant: 'Проект: Приложение для ресторана',
                projectElektronika24: 'Проект: Электроника24',
                projectServisComp: 'Сайт компании “Сервис Компьютер”',
                description: 'Создание сайта на WordPress, его администрирование, продвижение',
            },
            serviceEngineer: {
                title: 'Сервисный инженер:',
                placeWork: 'Компания: Сервис Компьютер',
                description: 'Специалист по установке, настройке, поиску неисправностей и ремонту высокотехнологичной техники.'
            }
        }
    },

    en: {
        subTitle: 'My About Details',
        title: 'About Me',
        description: `I am front-end developer with experience in
            creating SPA, using React, Redux, TypeScript.
            I am constantly developing my knowledge in depth and breadth, at the moment I develop the front-end part of the project, as well as actively studying the writing of the back-end. 
            I am accustomed to quality and responsible approach to my work, always bring it to an end.`,
        nameTabs: ['Main skills', 'Education', 'Work experience'],
        workExperience: {
            frontEnd: {
                title: 'My work as a Front-end developer:',
                projectCards: 'Project: Learning Cards',
                projectSocialNetwork: 'Project: Social Network',
                projectTodolist: 'Project: Todolist',
                projectRestaurant: 'Project: Restaurant app',
                projectElektronika24: 'Project: Elektronika24',
                projectServisComp: 'The website of the company "Service Computer”',
                description: 'WordPress website creation, administration, promotion',
            },
            serviceEngineer: {
                title: 'Service Engineer:',
                placeWork: 'Company: Service Computer',
                description: 'Specialist in the installation, setup, troubleshooting and repair of high-tech equipment'
            }
        }
    },

}