// @ts-ignore
import cvEn from '../assets/files/resumeEn.pdf'
// @ts-ignore
import cvRu from '../assets/files/resumeRu.pdf'

export const mainData = {
    ru: {
        subTitle: 'Привет',
        title: 'Я, Андрей Щавелев',
        typingEffect: 'Front-end разработчик',
        downloadCv: 'Скачать CV',
        linkCv: cvRu,
        footerName: 'Андрей Щавелев',
        copyright: 'Все права защищены'
    },

    en: {
        subTitle: 'Hi',
        title: 'I\'m Andrey Schavelev',
        typingEffect: 'Front-end Developer',
        downloadCv: 'DOWNLOAD CV',
        linkCv: cvEn,
        footerName: 'Andrey Schavelev',
        copyright: 'All rights reserved'
    },

}